% !TEX encoding = UTF-8 Unicode

% modular compilation
\usepackage{subfiles}

%bibstyle
\bibliographystyle{unsrt}

% load all required packages

% general
\usepackage[l2tabu,orthodox]{nag}  % force newer (and safer) LaTeX commands
\usepackage[utf8]{inputenc}        % set character set to UTF-8 (unicode)
\usepackage{babel}                 % multi-language support
%\usepackage{sectsty}               % allow redefinition of section command formatting
\usepackage{xcolor}                % more color options
%\usepackage{tocloft}               % redefine table of contents and define new like list
\usepackage{graphicx}
\usepackage{paralist}              % compactitem
\usepackage{listings}              % display shell commands and/or computer code

% overall graphic path(s)
\graphicspath{{\main/main/figures/}{\main/main/}}

% misc
%\usepackage{todonotes} % add to do notes
\usepackage{float}     % floats
\usepackage{makecell}

% science
\usepackage{amsmath}                                    % extensive math options
\usepackage{amssymb}                                    % special math symbols
\usepackage{xspace}                                     % spaces
%\usepackage[Gray,squaren,thinqspace,thinspace]{../sty/SIunits} % elegant units

% NEEDS to be before hyperref, hyperref
% number figures, tables and equations within the sections
%\numberwithin{equation}{section}
%\numberwithin{figure}{section}
%\numberwithin{table}{section}

% references and annotation
\usepackage[small,bf,hang]{caption}        % captions
\usepackage{subcaption}                    % adds subfigure, subcaption
\usepackage{hyperref}                      % add hyperlinks to references
\usepackage[noabbrev,nameinlink]{cleveref} % better references than default \ref
\usepackage{autonum}                       % only number referenced equations
\usepackage{url}                           % urls
\usepackage{lineno}                        % use line numbers

% format hyperlinks
\colorlet{linkcolour}{black}
\colorlet{urlcolour}{blue}
\hypersetup{colorlinks=true,linkcolor=linkcolour,citecolor=linkcolour,urlcolor=urlcolour}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\setlength{\textwidth}{18cm}
\setlength{\textheight}{24cm}
\setlength{\oddsidemargin}{-1cm}
\setlength{\oddsidemargin}{-1cm}
\setlength{\topmargin}{-2.8cm}

% do not indent first line of paragraph!                                                                                
%\setlength{\parindent}{0pt}                                                                                            
\usepackage{parskip}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% LHeC macros...
\def\subfilestableofcontents{\tableofcontents}

\def\biblio{
%  \bibliographystyle{unsrt}
  \renewcommand{\bibname}{Bibliography $\normalfont{\textrm{[please~read~the~footnote]}}$\,\footnote[999]{ \normalsize {\textbf Note on bibliography}:
      \begin{compactitem}
      \item Put all your `bibitems' into file \textit{../lhec.bib}. 
      \item Please use `inspirehep.net' BibTex entries, whenever possible and check for duplicates in the bib file.
      \item Do not forget to commit the updated .bib file to the git repository.
      \item Note: There will be only a single bibliography at the end of the CDR document. This bibliography section is only for editing purposes and disappears in the final document.
  \end{compactitem}} }
  \bibliography{\main/lhec}
} 

\def\lhecinstructions{
  \input{../main/instructions.tex}
}
\def\lhectitlepage{
  \input{../main/titlepage.tex}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

